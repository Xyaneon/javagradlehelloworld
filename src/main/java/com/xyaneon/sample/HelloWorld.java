package com.xyaneon.sample;

import javax.swing.*;

class HelloWorld {
    public static void main(String args[]){
        JFrame frame = new JFrame("Hello World");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,200);

        JButton button = new JButton("Press");
        frame.getContentPane().add(button);

        frame.setVisible(true);
    }
}
